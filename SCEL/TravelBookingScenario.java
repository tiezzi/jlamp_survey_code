package jlampSurveyPaper.jresp.example;


import java.io.IOException;
import java.util.Random;
import org.cmg.jresp.behaviour.Agent;
import org.cmg.jresp.comp.AttributeCollector;
import org.cmg.jresp.comp.Node;
import org.cmg.jresp.knowledge.ActualTemplateField;
import org.cmg.jresp.knowledge.FormalTemplateField;
import org.cmg.jresp.knowledge.Template;
import org.cmg.jresp.knowledge.Tuple;
import org.cmg.jresp.knowledge.ts.TupleSpace;
import org.cmg.jresp.topology.And;
import org.cmg.jresp.topology.Group;
import org.cmg.jresp.topology.GroupPredicate;
import org.cmg.jresp.topology.HasValue;
import org.cmg.jresp.topology.IsGreaterOrEqualThan;
import org.cmg.jresp.topology.IsLessOrEqualThan;
import org.cmg.jresp.topology.PointToPoint;
import org.cmg.jresp.topology.Self;
import org.cmg.jresp.topology.SocketPort;
import org.cmg.jresp.topology.SocketPortAddress;
import org.cmg.jresp.topology.Target;
import org.cmg.jresp.topology.VirtualPort;
import org.cmg.jresp.topology.VirtualPortAddress;
import jlampSurveyPaper.jresp.example.TravelBooking1.BrokerAgent;
import jlampSurveyPaper.jresp.example.TravelBooking1.CustomerAgent;
import jlampSurveyPaper.jresp.example.TravelBooking3.HotelAgent;


public class TravelBookingScenario {
	public static void main(String[] argv) throws IOException {
		VirtualPort vp = new VirtualPort(10);
		
		Node customerNode = new Node("customer", new TupleSpace());
		customerNode.addPort(vp);		
		Agent customerProcess = new CustomerAgent();
		customerNode.addAgent(customerProcess);
		
		Node brokerNode = new Node("broker", new TupleSpace());
		brokerNode.addPort(vp);
		Agent brokerProcess = new BrokerAgent(brokerNode);
		brokerNode.addAgent(brokerProcess);
		
		Node hotel1Node = new Node("hotel1", new TupleSpace());
		hotel1Node.addPort(vp);
		Agent hotel1Process = new HotelAgent(1,hotel1Node);
		hotel1Node.addAgent(hotel1Process);
		// Set serviceType attribute
		hotel1Node.put(new Tuple("serviceType","hotel"));
		hotel1Node.addAttributeCollector( new AttributeCollector("serviceType", 
				new Template( new ActualTemplateField( "serviceType"),
							new FormalTemplateField(String.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel1] serviceType="+t[0].getElementAt(String.class, 1));
				return t[0].getElementAt(String.class, 1);
			}
		});
		// Set locality attribute
		hotel1Node.put(new Tuple("locality","Camerino"));
		hotel1Node.addAttributeCollector( new AttributeCollector("locality", 
					new Template( new ActualTemplateField( "locality"),
							new FormalTemplateField(String.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel1] locality="+t[0].getElementAt(String.class, 1));
				return t[0].getElementAt(String.class, 1);
			}
		});
		// Set starRating attribute
		hotel1Node.put(new Tuple("starRating",4));
		hotel1Node.addAttributeCollector( new AttributeCollector("starRating", 
					new Template( new ActualTemplateField( "starRating"),
							new FormalTemplateField(Integer.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel1] starRating="+t[0].getElementAt(Integer.class, 1));
				return t[0].getElementAt(Integer.class, 1);
			}
		});		
		// Set roomPrice attribute
		hotel1Node.put(new Tuple("roomPrice",90.0));
		hotel1Node.addAttributeCollector( new AttributeCollector("roomPrice", 
					new Template( new ActualTemplateField( "roomPrice"),
							new FormalTemplateField(Double.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel1] roomPrice="+t[0].getElementAt(Double.class, 1));
				return t[0].getElementAt(Double.class, 1);
			}
		});		
		
		
		
		Node hotel2Node = new Node("hotel2", new TupleSpace());
		hotel2Node.addPort(vp);
		Agent hotel2Process = new HotelAgent(2,hotel2Node);
		hotel2Node.addAgent(hotel2Process);
		// Set serviceType attribute
		hotel2Node.put(new Tuple("serviceType","hotel"));
		hotel2Node.addAttributeCollector( new AttributeCollector("serviceType", 
				new Template( new ActualTemplateField( "serviceType"),
							new FormalTemplateField(String.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[Hotel2] serviceType="+t[0].getElementAt(String.class, 1));
				return t[0].getElementAt(String.class, 1);
			}
		});
		// Set locality attribute
		hotel2Node.put(new Tuple("locality","Camerino"));
		hotel2Node.addAttributeCollector( new AttributeCollector("locality", 
					new Template( new ActualTemplateField( "locality"),
							new FormalTemplateField(String.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[Hotel2] locality="+t[0].getElementAt(String.class, 1));
				return t[0].getElementAt(String.class, 1);
			}
		});
		// Set starRating attribute
		hotel2Node.put(new Tuple("starRating",3));
		hotel2Node.addAttributeCollector( new AttributeCollector("starRating", 
					new Template( new ActualTemplateField( "starRating"),
							new FormalTemplateField(Integer.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel2] starRating="+t[0].getElementAt(Integer.class, 1));
				return t[0].getElementAt(Integer.class, 1);
			}
		});		
		// Set roomPrice attribute
		hotel2Node.put(new Tuple("roomPrice",110.0));
		hotel2Node.addAttributeCollector( new AttributeCollector("roomPrice", 
					new Template( new ActualTemplateField( "roomPrice"),
							new FormalTemplateField(Double.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel2] roomPrice="+t[0].getElementAt(Double.class, 1));
				return t[0].getElementAt(Double.class, 1);
			}
		});		
		
		
		
		Node hotel3Node = new Node("hotel3", new TupleSpace());
		hotel3Node.addPort(vp);
		Agent hotel3Process = new HotelAgent(3,hotel3Node);
		hotel3Node.addAgent(hotel3Process);
		// Set serviceType attribute
		hotel3Node.put(new Tuple("serviceType","hotel"));
		hotel3Node.addAttributeCollector( new AttributeCollector("serviceType", 
				new Template( new ActualTemplateField( "serviceType"),
							new FormalTemplateField(String.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel3] serviceType="+t[0].getElementAt(String.class, 1));
				return t[0].getElementAt(String.class, 1);
			}
		});
		// Set locality attribute
		hotel3Node.put(new Tuple("locality","Camerino"));
		hotel3Node.addAttributeCollector( new AttributeCollector("locality", 
					new Template( new ActualTemplateField( "locality"),
							new FormalTemplateField(String.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel3] locality="+t[0].getElementAt(String.class, 1));
				return t[0].getElementAt(String.class, 1);
			}
		});
		// Set starRating attribute
		hotel3Node.put(new Tuple("starRating",3));
		hotel3Node.addAttributeCollector( new AttributeCollector("starRating", 
					new Template( new ActualTemplateField( "starRating"),
							new FormalTemplateField(Integer.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel3] starRating="+t[0].getElementAt(Integer.class, 1));
				return t[0].getElementAt(Integer.class, 1);
			}
		});		
		// Set roomPrice attribute
		hotel3Node.put(new Tuple("roomPrice",80.0));
		hotel3Node.addAttributeCollector( new AttributeCollector("roomPrice", 
					new Template( new ActualTemplateField( "roomPrice"),
							new FormalTemplateField(Double.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel3] roomPrice="+t[0].getElementAt(Double.class, 1));
				return t[0].getElementAt(Double.class, 1);
			}
		});		
		
		
		
		Node hotel4Node = new Node("hotel4", new TupleSpace());
		hotel4Node.addPort(vp);
		Agent hotel4Process = new HotelAgent(4,hotel4Node);
		hotel4Node.addAgent(hotel4Process);
		// Set serviceType attribute
		hotel4Node.put(new Tuple("serviceType","hotel"));
		hotel4Node.addAttributeCollector( new AttributeCollector("serviceType", 
				new Template( new ActualTemplateField( "serviceType"),
							new FormalTemplateField(String.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel4] serviceType="+t[0].getElementAt(String.class, 1));
				return t[0].getElementAt(String.class, 1);
			}
		});
		// Set locality attribute
		hotel4Node.put(new Tuple("locality","Pisa"));
		hotel4Node.addAttributeCollector( new AttributeCollector("locality", 
					new Template( new ActualTemplateField( "locality"),
							new FormalTemplateField(String.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel4] locality="+t[0].getElementAt(String.class, 1));
				return t[0].getElementAt(String.class, 1);
			}
		});
		// Set starRating attribute
		hotel4Node.put(new Tuple("starRating",5));
		hotel4Node.addAttributeCollector( new AttributeCollector("starRating", 
					new Template( new ActualTemplateField( "starRating"),
							new FormalTemplateField(Integer.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel4] starRating="+t[0].getElementAt(Integer.class, 1));
				return t[0].getElementAt(Integer.class, 1);
			}
		});		
		// Set roomPrice attribute
		hotel4Node.put(new Tuple("roomPrice",95.0));
		hotel4Node.addAttributeCollector( new AttributeCollector("roomPrice", 
					new Template( new ActualTemplateField( "roomPrice"),
							new FormalTemplateField(Double.class) )) {
			@Override
			protected Object doEval(Tuple ... t) {
				//System.out.println("[hotel4] roomPrice="+t[0].getElementAt(Double.class, 1));
				return t[0].getElementAt(Double.class, 1);
			}
		});		
		
		
		
		
		
		brokerNode.start();
		customerNode.start();
		hotel1Node.start();
		hotel2Node.start();
		hotel3Node.start();
		hotel4Node.start();
	}

	public static class CustomerAgent extends Agent {

		PointToPoint broker = new PointToPoint("broker", new VirtualPortAddress(10));
		PointToPoint self = new PointToPoint("customer", new VirtualPortAddress(10));

		public CustomerAgent() {
			super("CustomerProcess");
		}

		@Override
		protected void doRun() {
			try {
				String key = fresh();
				System.out.println("[Customer] Fresh key generated: "+key);
				
				String loc = "Camerino";
				String day = "8 October 2019";
				Integer rating = 2;
				double price = 100.0;
				
				System.out.println("[Customer] Send Acms");
				put(new Tuple("Acms",loc,day,rating,price,self,key), broker);
				
				boolean offerIsUnsatisfactory = true;
				while (offerIsUnsatisfactory) {
					Tuple offer = get(new Template(new ActualTemplateField("Offer"),
							  new ActualTemplateField(day),
					          new FormalTemplateField(String.class),
					          new FormalTemplateField(Double.class),
					          new FormalTemplateField(Target.class),
					          new ActualTemplateField(key)), Self.SELF);
					System.out.println("[Customer] Offer received: "+
							  offer.getElementAt(String.class,1)+", "+
					          offer.getElementAt(String.class,2)+", "+
					          offer.getElementAt(Double.class,3)+", "+
					          offer.getElementAt(Target.class,4)+", "+
					          offer.getElementAt(String.class,5)+" ");
				    String f = offer.getElementAt(String.class,2);
				    Double p = offer.getElementAt(Double.class,3);
				    Target h = offer.getElementAt(Target.class,4);
				    if (checkOffer(f,p,price)) {
				    	offerIsUnsatisfactory = false;
				    	System.out.println("[Customer] Book the room");
				    	put(new Tuple("Book",day,f,key), h);
				    	
				    	// Get confirmation from the broker
				        Tuple confirmation = get(new Template(new ActualTemplateField("Confirm"),
						          new ActualTemplateField(day),
						          new ActualTemplateField(f),
						          new ActualTemplateField(h),
						          new ActualTemplateField(key)), Self.SELF);
				    	System.out.println("[Customer] Confirmation received");							    	
				    } else {
				    	System.out.println("[Customer] Offer not accepted");
				    }					
				}
				System.out.println("[Customer] Execution completed");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private boolean checkOffer(String f,Double offeredPrice,double desiredPrice) {
			return desiredPrice >= offeredPrice;
		}
		
		

	}

	public static class BrokerAgent extends Agent {

		private Node brokerNode;
		
		public BrokerAgent(Node node) {
			super("BrokerProcess");
			brokerNode=node;
		}

		@Override
		protected void doRun() {
			try {
				    System.out.println("[Broker] instance activated");
					Tuple acms = get(new Template(new ActualTemplateField("Acms"),
							          new FormalTemplateField(String.class),
							          new FormalTemplateField(String.class),
							          new FormalTemplateField(Integer.class),
							          new FormalTemplateField(Double.class),
							          new FormalTemplateField(Target.class),
							          new FormalTemplateField(String.class)), Self.SELF);
					System.out.println("[Broker] Acms received: "+
							          acms.getElementAt(String.class,1)+", "+
							          acms.getElementAt(String.class,2)+", "+
							          acms.getElementAt(Integer.class,3)+", "+
							          acms.getElementAt(Double.class,4)+", "+
							          acms.getElementAt(Target.class,5)+", "+
							          acms.getElementAt(String.class,6)+" ");
					
					String l = acms.getElementAt(String.class,1);
			        String d = acms.getElementAt(String.class,2);
			        Integer r = acms.getElementAt(Integer.class,3);
			        Double p = acms.getElementAt(Double.class,4);
			        Target c= acms.getElementAt(Target.class,5);
			        String k = acms.getElementAt(String.class,6);
			        
			        // Spawning, for parallel execution, a copy of itself to serve other inquiries
			        Agent B = new BrokerAgent(brokerNode);
			        brokerNode.addAgent(B);
			        
					// Set the ensemble predicate
			        GroupPredicate p1 = new HasValue("serviceType","hotel");
			        GroupPredicate p2 = new HasValue("locality",l);
			        GroupPredicate p3 = new IsGreaterOrEqualThan("starRating", r);
			        GroupPredicate p4 = new IsLessOrEqualThan("roomPrice", p);
			        GroupPredicate predicate = new And(new And(new And(p1,p2),p3),p4);
			        
			        // Group-oriented put to hotels 
			        put(new Tuple("Acms",d,c,k), new Group(predicate));
			        
			        // Get confirmation from an hotel
			        Tuple confirmation = get(new Template(new ActualTemplateField("Confirm"),
					          new ActualTemplateField(d),
					          new FormalTemplateField(String.class),
					          new FormalTemplateField(Target.class),
					          new ActualTemplateField(k)), Self.SELF);			
			        System.out.println("[Broker] Confirmation received");
			        String f = confirmation.getElementAt(String.class,2);
			        Target h= confirmation.getElementAt(Target.class,3);
			        
			        // Send confirmation to the customer
			        System.out.println("[Broker] Send confirmation");
			        put(new Tuple("Confirm",d,f,h,k),c); 
			        System.out.println("[Broker] Instace execution completed");
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}


	
	
	
	
	public static class HotelAgent extends Agent {
		private int hotelId;
		private PointToPoint self;
		private Node hotelNode;
		
		PointToPoint broker = new PointToPoint("broker", new VirtualPortAddress(10));
		
		public HotelAgent(Integer i, Node node) {
			super("Hotel"+i+"Process");
			this.hotelId = i;
			self = new PointToPoint("hotel"+i, new VirtualPortAddress(10));
			hotelNode = node;
		}

		@Override
		protected void doRun() {
			try {
				
				    System.out.println("[Hotel"+hotelId+"] instance activated");
					Tuple acms = get(new Template(new ActualTemplateField("Acms"),
							          new FormalTemplateField(String.class),
							          new FormalTemplateField(Target.class),
							          new FormalTemplateField(String.class)), Self.SELF);
					System.out.println("[Hotel"+hotelId+"] Acms received: "+
							          acms.getElementAt(String.class,1)+", "+
							          acms.getElementAt(Target.class,2)+", "+
							          acms.getElementAt(String.class,3)+" ");
					
					String d = acms.getElementAt(String.class,1);
			        Target c= acms.getElementAt(Target.class,2);
			        String k = acms.getElementAt(String.class,3);
			        
			        String f = "featuresHotel"+hotelId;
			        
			        // Spawning, for parallel execution, a copy of itself to serve other inquiries
			        Agent H = new HotelAgent(hotelId,hotelNode);
			        hotelNode.addAgent(H);
			        
			        
			        // Retrieve the room price
			        Tuple roomPrice = query(new Template(new ActualTemplateField("roomPrice"),
							          new FormalTemplateField(Double.class)), Self.SELF);
			        double p = roomPrice.getElementAt(Double.class, 1);
			        System.out.println("[Hotel"+hotelId+"] Retrieved price: "+p);
					
					// The hotel makes always an offer
			        System.out.println("[Hotel"+hotelId+"] Send Offer");					
					put(new Tuple("Offer",d,f,p,self,k),c);
					
					// Get the book request
			        Tuple book= query(new Template(new ActualTemplateField("Book"),
			        		          new ActualTemplateField(d),
			        		          new ActualTemplateField(f),
			        		          new ActualTemplateField(k)), Self.SELF);
			        System.out.println("[Hotel"+hotelId+"] Booking request received: "+
			        		book.getElementAt(String.class,1)+", "+
			        		book.getElementAt(String.class,2)+", "+
			        		book.getElementAt(String.class,3)+" ");
					
					// Send confirmation to the broker
			        System.out.println("[Hotel"+hotelId+"] Send confirmation");
			        put(new Tuple("Confirm",d,f,self,k),broker);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}
