
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Random;
import org.cmg.jresp.behaviour.Agent;
import org.cmg.jresp.comp.AttributeCollector;
import org.cmg.jresp.comp.Node;
import org.cmg.jresp.comp.NodeConnection;
import org.cmg.jresp.knowledge.ActualTemplateField;
import org.cmg.jresp.knowledge.FormalTemplateField;
import org.cmg.jresp.knowledge.Template;
import org.cmg.jresp.knowledge.Tuple;
import org.cmg.jresp.knowledge.ts.TupleSpace;
import org.cmg.jresp.simulation.DeterministicDelayFactory;
import org.cmg.jresp.simulation.ExponentialDelayFactory;
import org.cmg.jresp.simulation.RandomSelector;
import org.cmg.jresp.simulation.SimulationAction;
import org.cmg.jresp.simulation.SimulationEnvironment;
import org.cmg.jresp.simulation.SimulationNode;
import org.cmg.jresp.simulation.SimulationNodeAddress;
import org.cmg.jresp.simulation.SimulationScheduler;
import org.cmg.jresp.topology.And;
import org.cmg.jresp.topology.Group;
import org.cmg.jresp.topology.GroupPredicate;
import org.cmg.jresp.topology.HasValue;
import org.cmg.jresp.topology.IsGreaterOrEqualThan;
import org.cmg.jresp.topology.IsLessOrEqualThan;
import org.cmg.jresp.topology.PointToPoint;
import org.cmg.jresp.topology.Self;
import org.cmg.jresp.topology.SocketPort;
import org.cmg.jresp.topology.SocketPortAddress;
import org.cmg.jresp.topology.Target;
import org.cmg.jresp.topology.VirtualPort;
import org.cmg.jresp.topology.VirtualPortAddress;

public class MC {
	static boolean goalReached = false;
	static String[] localitiesStrings = {"Camerino","Pisa","Firenze","Lucca"};
	static Random random = new Random();

	public static void main(String[] argv) throws IOException, InterruptedException {
		double deadline = 20.0;
		double dt = 0.1;
		int iterations = 100;
		int numOfHotels = 25;

		HashMap<Double, Integer> results = new HashMap<Double, Integer>();

		SimulationAction action = new SimulationAction() {

			@Override
			public void doAction(double time) {
				if (goalReached) {
					Integer value = results.get(time);
					if (value == null) {
						value = 0;
					}
					results.put(time, value + 1);
				}
			}
		};

		for (int i = 0; i < iterations; i++) {
			long start = System.currentTimeMillis();

			goalReached = false;
			Random r = new Random();
			SimulationScheduler sim = new SimulationScheduler();
			SimulationEnvironment env = new SimulationEnvironment(sim, new RandomSelector(r),
					new ExponentialDelayFactory(r,1.0), new NodeConnection() {
						@Override
						public void waitInTouch(String arg0, String arg1) throws InterruptedException {}

						@Override
						public boolean areInTouch(String arg0, String arg1) {
							return true;
						}
					});

			SimulationNode customerNode = new SimulationNode("customer", env);
			Agent customerProcess = new CustomerAgent();
			customerNode.addAgent(customerProcess);

			SimulationNode brokerNode = new SimulationNode("broker", env);
			Agent brokerProcess = new BrokerAgent(brokerNode);
			brokerNode.addAgent(brokerProcess);


			SimulationNode[] hotels = new SimulationNode[numOfHotels];

			for (int j = 0; j < numOfHotels; j++) {
				SimulationNode hotelNode = new SimulationNode("hotel"+j, env);
				Agent hotelProcess = new HotelAgent(j, hotelNode);
				hotelNode.addAgent(hotelProcess);
				// Set serviceType attribute
				hotelNode.put(new Tuple("serviceType", "hotel"));
				hotelNode.addAttributeCollector(new AttributeCollector("serviceType",
						new Template(new ActualTemplateField("serviceType"), new FormalTemplateField(String.class))) {
					@Override
					protected Object doEval(Tuple... t) {
						return t[0].getElementAt(String.class, 1);
					}
				});
				// Set locality attribute
				hotelNode.put(new Tuple("locality", getLocality()));
				hotelNode.addAttributeCollector(new AttributeCollector("locality",
						new Template(new ActualTemplateField("locality"), new FormalTemplateField(String.class))) {
					@Override
					protected Object doEval(Tuple... t) {
						return t[0].getElementAt(String.class, 1);
					}
				});
				// Set starRating attribute
				hotelNode.put(new Tuple("starRating", getStars()));
				hotelNode.addAttributeCollector(new AttributeCollector("starRating",
						new Template(new ActualTemplateField("starRating"), new FormalTemplateField(Integer.class))) {
					@Override
					protected Object doEval(Tuple... t) {
						return t[0].getElementAt(Integer.class, 1);
					}
				});
				// Set roomPrice attribute
				hotelNode.put(new Tuple("roomPrice", getPrice()));
				hotelNode.addAttributeCollector(new AttributeCollector("roomPrice",
						new Template(new ActualTemplateField("roomPrice"), new FormalTemplateField(Double.class))) {
					@Override
					protected Object doEval(Tuple... t) {
						return t[0].getElementAt(Double.class, 1);
					}
				});

				hotels[j] = hotelNode;
			}

			env.schedulePeriodicAction(action, 0.0, dt);
			env.simulate(deadline);
			env.join();
			long end = System.currentTimeMillis();
			System.out.println("\n\nIteration " + i + " completed!");
			System.out.println("Iteration time: " + (end - start) + "\n\n");
		}

		PrintWriter writer = new PrintWriter("/Users/tiezzi/Desktop/JRESP_DATA/data_"+numOfHotels+".dat");
		double count = 0.0;
		while (count <= deadline) {
			Integer value = results.get(count);
			double prob = 0.0;
			if (value != null) {
				prob = value.intValue();
				prob = prob / iterations;
			}
			writer.println(count + "\t" + prob);
			count += dt;
		}
		writer.close();
		System.out.println("THE END");

	}

   private static String getLocality() {
	   int r = random.nextInt(4);
	   return localitiesStrings[r];
   }

   private static int getStars() {
	   Random random = new Random();
	   int r = random.nextInt(4);
	   return r+2;
   }

   private static double getPrice() {
	   double basePrice = 70.0;
	   Random random = new Random();
	   int offset = random.nextInt(70);
	   return basePrice+offset;
   }

	public static class CustomerAgent extends Agent {

		PointToPoint broker = new PointToPoint("broker", SimulationNodeAddress.getInstance());
		PointToPoint self = new PointToPoint("customer", SimulationNodeAddress.getInstance());

		public CustomerAgent() {
			super("CustomerProcess");
		}

		@Override
		protected void doRun() {
			try {
				String key = fresh();
				System.out.println("[Customer] Fresh key generated: " + key);

				String loc = "Camerino";
				String day = "8 October 2019";
				Integer rating = 2;
				double price = 100.0;

				System.out.println("[Customer] Send Acms");
				put(new Tuple("Acms", loc, day, rating, price, self, key), broker);

				boolean offerIsUnsatisfactory = true;
				while (offerIsUnsatisfactory) {
					Tuple offer = get(new Template(new ActualTemplateField("Offer"), new ActualTemplateField(day),
							new FormalTemplateField(String.class), new FormalTemplateField(Double.class),
							new FormalTemplateField(Target.class), new ActualTemplateField(key)), Self.SELF);
					System.out.println("[Customer] Offer received: " + offer.getElementAt(String.class, 1) + ", "
							+ offer.getElementAt(String.class, 2) + ", " + offer.getElementAt(Double.class, 3) + ", "
							+ offer.getElementAt(Target.class, 4) + ", " + offer.getElementAt(String.class, 5) + " ");
					String f = offer.getElementAt(String.class, 2);
					Double p = offer.getElementAt(Double.class, 3);
					Target h = offer.getElementAt(Target.class, 4);
					if (checkOffer(f, p, price)) {
						offerIsUnsatisfactory = false;
						System.out.println("[Customer] Book the room");
						put(new Tuple("Book", day, f, key), h);

						// Get confirmation from the broker
						Tuple confirmation = get(new Template(new ActualTemplateField("Confirm"),
								new ActualTemplateField(day), new ActualTemplateField(f), new ActualTemplateField(h),
								new ActualTemplateField(key)), Self.SELF);
						System.out.println("[Customer] Confirmation received");
					} else {
						System.out.println("[Customer] Offer not accepted");
					}
				}
				System.out.println("[Customer] Execution completed");
				goalReached = true;
			} catch (Exception e) {
			}
		}

		private boolean checkOffer(String f, Double offeredPrice, double desiredPrice) {
			return desiredPrice >= offeredPrice;
		}

	}

	public static class BrokerAgent extends Agent {

		private SimulationNode brokerNode;

		public BrokerAgent(SimulationNode node) {
			super("BrokerProcess");
			brokerNode = node;
		}

		@Override
		protected void doRun() {
			try {
				System.out.println("[Broker] instance activated");

				Tuple acms = get(new Template(new ActualTemplateField("Acms"), new FormalTemplateField(String.class),
						new FormalTemplateField(String.class), new FormalTemplateField(Integer.class),
						new FormalTemplateField(Double.class), new FormalTemplateField(Target.class),
						new FormalTemplateField(String.class)), Self.SELF);
				System.out.println("[Broker] Acms received: " + acms.getElementAt(String.class, 1) + ", "
						+ acms.getElementAt(String.class, 2) + ", " + acms.getElementAt(Integer.class, 3) + ", "
						+ acms.getElementAt(Double.class, 4) + ", " + acms.getElementAt(Target.class, 5) + ", "
						+ acms.getElementAt(String.class, 6) + " ");

				String l = acms.getElementAt(String.class, 1);
				String d = acms.getElementAt(String.class, 2);
				Integer r = acms.getElementAt(Integer.class, 3);
				Double p = acms.getElementAt(Double.class, 4);
				Target c = acms.getElementAt(Target.class, 5);
				String k = acms.getElementAt(String.class, 6);

				// Set the ensemble predicate
				GroupPredicate p1 = new HasValue("serviceType", "hotel");
				GroupPredicate p2 = new HasValue("locality", l);
				GroupPredicate p3 = new IsGreaterOrEqualThan("starRating", r);
				GroupPredicate p4 = new IsLessOrEqualThan("roomPrice", p);
				GroupPredicate predicate = new And(new And(new And(p1, p2), p3), p4);

				// Group-oriented put to hotels
				put(new Tuple("Acms", d, c, k), new Group(predicate));

				// Get confirmation from an hotel
				Tuple confirmation = get(new Template(new ActualTemplateField("Confirm"), new ActualTemplateField(d),
						new FormalTemplateField(String.class), new FormalTemplateField(Target.class),
						new ActualTemplateField(k)), Self.SELF);
				System.out.println("[Broker] Confirmation received");
				String f = confirmation.getElementAt(String.class, 2);
				Target h = confirmation.getElementAt(Target.class, 3);

				// Send confirmation to the customer
				System.out.println("[Broker] Send confirmation");
				put(new Tuple("Confirm", d, f, h, k), c);
				System.out.println("[Broker] Instace execution completed");

			} catch (Exception e) {
			}
		}

	}

	public static class HotelAgent extends Agent {
		private int hotelId;
		private PointToPoint self;
		private SimulationNode hotelNode;

		PointToPoint broker = new PointToPoint("broker", SimulationNodeAddress.getInstance());




		public HotelAgent(Integer i, SimulationNode node) {
			super("Hotel" + i + "Process");
			this.hotelId = i;
			self = new PointToPoint("hotel" + i, SimulationNodeAddress.getInstance());
			hotelNode = node;
		}

		@Override
		protected void doRun() {
			try {

				System.out.println("[Hotel" + hotelId + "] instance activated");
				Tuple acms = get(
						new Template(new ActualTemplateField("Acms"), new FormalTemplateField(String.class),
								new FormalTemplateField(Target.class), new FormalTemplateField(String.class)),
						Self.SELF);
				System.out.println("[Hotel" + hotelId + "] Acms received: " + acms.getElementAt(String.class, 1) + ", "
						+ acms.getElementAt(Target.class, 2) + ", " + acms.getElementAt(String.class, 3) + " ");

				String d = acms.getElementAt(String.class, 1);
				Target c = acms.getElementAt(Target.class, 2);
				String k = acms.getElementAt(String.class, 3);

				String f = "featuresHotel" + hotelId;

			// Retrieve the room price
				Tuple roomPrice = query(
						new Template(new ActualTemplateField("roomPrice"), new FormalTemplateField(Double.class)),
						Self.SELF);
				double p = roomPrice.getElementAt(Double.class, 1);
				System.out.println("[Hotel" + hotelId + "] Retrieved price: " + p);

				// The hotel makes always an offer
				System.out.println("[Hotel" + hotelId + "] Send Offer");
				put(new Tuple("Offer", d, f, p, self, k), c);

				// Get the book request
				Tuple book = query(new Template(new ActualTemplateField("Book"), new ActualTemplateField(d),
						new ActualTemplateField(f), new ActualTemplateField(k)), Self.SELF);
				System.out.println(
						"[Hotel" + hotelId + "] Booking request received: " + book.getElementAt(String.class, 1) + ", "
								+ book.getElementAt(String.class, 2) + ", " + book.getElementAt(String.class, 3) + " ");

				// Send confirmation to the broker
				System.out.println("[Hotel" + hotelId + "] Send confirmation");
				put(new Tuple("Confirm", d, f, self, k), broker);

			} catch (Exception e) {
			}
		}

	}
}
