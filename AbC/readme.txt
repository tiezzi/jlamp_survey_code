*** INFO ABOUT ABEL CODE  ***

The repository about the ABEL code contains the following files:

- alphatv.abc : the Travel Booking scenario written in AbC style; 

- cust.erl, hotel.erl, broker.erl : automatically generated Erlang code by the tool (each file for each type of component in the scenario, i.e. customer, hotel and broker);

- alphatv.erl, user_code.erl : the user code for the scenario instantiation and helper functions, respectively.

The code can be executed with Erlang and ABEL library installed.

Source files, binary files and documentation about ABEL are available at the following repository:

https://github.com/ArBITRAL/SG65

 
