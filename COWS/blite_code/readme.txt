*** Blite quick start guide ***

* Compilation *
BliteC generates a .bpr file from a Blite program by means of the following command:

   java -jar blitec.jar fileName.bl


* Deployment *
The generated .bpr files has to be moved to the "bpr" directory of Apache Tomcat where the ActiveBPEL engine is installed (we have used the version 5.0.28 of Jakarta Tomcat).

The console of ActiveBPEL is accessible from: 

   http://localhost:8080/BpelAdmin/

